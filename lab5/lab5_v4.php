<?php
abstract class Value {
    protected $data;
    public function __construct($data) {
        $this->data = $data;
    }
    public function set_data($data) {
        $this->data = $data;
    }
    public function get_data($data) {
        return $this->data;
    }
    abstract public function calc_pressure() : float;
}
class Pascal extends Value {
    public function calc_pressure() : float {
        return $this->data * 9.8692 * 10**-6;
    }
}
class Bar extends Value {
    public function calc_pressure() : float {
        return $this->data * 9.8692 * 10**-1;
    }
}
class MMWC extends Value {
    public function calc_pressure() : float {
        return $this->data * 760;
    }
}

function make_values($data) {
    $values = array();
    foreach ($data as $pressure) {
        switch ($pressure[0]) {
        case 'p':
            array_push($values, [new Pascal($pressure[1]), "From pascal (" . $pressure[1] . ") to atmosphere: "]);
            break;
        case 'b':
            array_push($values, [new Bar($pressure[1]), "From bar (" . $pressure[1] . ") to atmosphere: "]);
            break;
        case 'm':
            array_push($values, [new MMWC($pressure[1]), "From mmwc (" . $pressure[1] . ") to atmosphere: "]);
            break;
        default:
            break;
        }
    }
    return $values;
}
    
$data = [ ['p', 345]
        , ['b', 487]
        , ['m', 64] ];
$values = make_values($data);
foreach ($values as $value) {
    echo $value[1] . $value[0]->calc_pressure() . "\n";
}
?>
