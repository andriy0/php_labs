<?php
abstract class Value {
    protected $data;
    public function __construct($data) {
        $this->data = $data;
    }
    public function set_data($data) {
        $this->data = $data;
    }
    public function get_data($data) {
        return $this->data;
    }
    abstract public function calc_pressure() : float;
}
class Pascal extends Value {
    public function calc_pressure() : float {
        return $this->data * 9.8692 * 10**-6;
    }
}
class Bar extends Value {
    public function calc_pressure() : float {
        return $this->data * 9.8692 * 10**-1;
    }
}
class MMWC extends Value {
    public function calc_pressure() : float {
        return $this->data * 760;
    }
}

function make_values($data) {
    $values = array();
    foreach ($data as $pressure) {
        switch ($pressure[0]) {
        case 'p':
            array_push($values, [new Pascal($pressure[1]), "From pascal (" . $pressure[1] . ") to atmosphere: "]);
            break;
        case 'b':
            array_push($values, [new Bar($pressure[1]), "From bar (" . $pressure[1] . ") to atmosphere: "]);
            break;
        case 'm':
            array_push($values, [new MMWC($pressure[1]), "From mmwc (" . $pressure[1] . ") to atmosphere: "]);
            break;
        default:
            break;
        }
    }
    return $values;
}
    
function get_data($filename) {
    $data = array();
    $infile = fopen($filename, "r") or die("Unable to open file!");
    while(!feof($infile)) {
        $pre_data = explode(" : ", fgets($infile));
        array_push($data, [$pre_data[0], floatval($pre_data[1])]);
    }
    fclose($infile);
    return $data;
}

function write_data($filename, $values) {
    $outfile = fopen($filename, "w") or die("Unable to open file!");
    foreach ($values as $value) {
        fwrite($outfile, $value[1] . $value[0]->calc_pressure() . "\n");
    }
    fclose($outfile);
}
    

write_data("output.txt", make_values(get_data("input.txt")));
?>
