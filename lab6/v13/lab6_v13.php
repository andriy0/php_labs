<?php
abstract class Shape {
    protected $data;
    public function __construct($data) {
        $this->data = $data;
    }
    public function set_data($data) {
        $this->data = $data;
    }
    public function get_data($data) {
        return $this->data;
    }
    abstract protected function calc_perimeter() : float;
    public function calc_rotations() : float {
        return $this->data[3] / $this->calc_perimeter();
    }
}
class Ellipsis extends Shape {
    protected function calc_perimeter() : float {
        return 4 *
            (pi()*$this->data[1]*$this->data[2] +
             ($this->data[1]-$this->data[2])**2) /
            $this->data[1]+$this->data[2];
    }
}
class Circle extends Shape {
    protected function calc_perimeter() : float {
        return 2*pi()*$this->data[1];
    }
}
class Rhombus extends Shape {
    protected function calc_perimeter() : float {
        $a = sqrt((2*$this->data[1])**2 +
                     (2*$this->data[2])**2) /
           2;
        return 4*$a;
    }
}

function make_shapes($data) {
    $shapes = array();
    foreach ($data as $shape_props) {
        switch ($shape_props[0]) {
        case 'e':
            array_push($shapes, new Ellipsis($shape_props));
            break;
        case 'c':
            array_push($shapes, new Circle($shape_props));
            break;
        case 'r':
            array_push($shapes, new Rhombus($shape_props));
            break;
        default:
            break;
        }
    }
    return $shapes;
}

function get_data($filename) {
    $data = array();
    $infile = fopen($filename, "r") or die("Unable to open file!");
    while(!feof($infile)) {
        $pre_data = explode(" : ", fgets($infile));
        array_push($data, array_merge([$pre_data[0]], array_map('floatval', explode(" ", $pre_data[1]))));
    }
    fclose($infile);
    return $data;
}

function write_data($filename, $shapes) {
    $outfile = fopen($filename, "w") or die("Unable to open file!");
    foreach ($shapes as $shape) {
        fwrite($outfile, $shape->calc_rotations() . "\n");
    }
    fclose($outfile);
}


write_data("output.txt", make_shapes(get_data("input.txt")));
?>
