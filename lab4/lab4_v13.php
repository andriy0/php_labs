<?php
//first task
function task_one($r, $a, $b) {
    $t = 0;
    if($r >= $a){
        $t = ($a**2 + $r**(1/3)) / log($r + $b)**2;
        return $t;
    }
    if($r < $a/2 and $r >= $a/3){
        $t = sqrt($a * exp($r)) / $r;
        return $t;
    }
}

//second task
function &task_two($a, $start, $step, $n) {
    $y = $start;
    $res[$n];
    for($i = 0; $i < $n; $i++) {
        $z = ((atan($a + $y)**3) / (log($a)**2)) + (pi() / 6*$a);
        $res[$i] = $z;
        $y += $step;
    }
    return $res;
}

echo("Task one:\n");
echo(task_one(8.269, 6.347, 21.4) . "\n");
echo(task_one(2.892, 6.347, 21.4) . "\n");

echo("\n");

echo("Task two:\n");
$res =& task_two(3.2, -4.8, 0.1, 7);
for($i = 0; $i < 7; $i++) {
    echo($res[$i] . "\n");
}
?>
