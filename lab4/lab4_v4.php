<?php
//first task
function task_one($x, $a, $b) {
    $d = 0;
    if($x == $b){
        $d = $x*sin($b*$x + pi()/10);
        return $d;
    }
    if($x <= $a and $x > $b){
        $d = $b*cos($x**2)**2;
        return $d;
    }
}

//second task
function &task_two($a, $b, $stop, $step, $n) {
    $start = $stop - $step*$n;
    $m = $start;
    $res[$n];
    for($i = 0; $i < $n; $i++) {
        $t = $m**$a + sqrt($m + ($b**2*$m) / log(exp(1), 2));
        $res[$i] = $t;
        $m += $step;
    }
    return $res;
}

echo("Task one:\n");
echo(task_one(1.321, 3.65, 1.321) . "\n");
echo(task_one(2.65, 3.65, 1.321) . "\n");

echo("\n");

echo("Task two:\n");
$res =& task_two(1.6, 2.09, 16, 1.5, 7);
for($i = 0; $i < 7; $i++) {
    echo($res[$i] . "\n");
}
?>
