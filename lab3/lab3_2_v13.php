<?php
//second task
$a = 3.2;
$start = -4.8;
$step = 0.1;
$n = 7;
$y = $start;
echo("Task two:\n");
for($i = 0; $i < $n; $i++) {
    $z = ((atan($a + $y)**3) / (log($a)**2)) + (pi() / 6*$a);
    echo($z . "\n");
    $y += $step;
}
?>
