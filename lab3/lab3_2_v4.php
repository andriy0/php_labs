<?php
//second task
$a = 1.6;
$b = 2.09;
$stop = 16;
$step = 1.5;
$n = 7;
$start = $stop - $step*$n;
$m = $start;
echo("Task two:\n");
for($i = 0; $i < $n; $i++) {
    $t = $m**$a + sqrt($m + ($b**2*$m) / log(exp(1), 2));
    echo($t . "\n");
    $m += $step;
}
?>
